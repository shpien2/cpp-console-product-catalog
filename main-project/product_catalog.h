#pragma once
#ifndef PRODUCT_CATALOG_H
#define PRODUCT_CATALOG_H

#include "constants.h"

struct product_catalog
{
    double cost;
    int kol;
    char category[MAX_STRING_SIZE];
    char name[MAX_STRING_SIZE];
};

#endif