#include <iostream>

using namespace std;

#include "product_catalog.h"
#include "file_reader.h"
#include "constants.h"


int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �10. ������� �������\n";
    cout << "�����: ���� �����\n\n";
    product_catalog* products[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", products, size);
        cout << "***** ������� ������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /******** ����� ��������� *********/
            cout << "���������...: ";
            cout << products[i]->cost << '\n';
            /******** ����� ���������� ********/
            cout << "����������...: ";
            cout << products[i]->kol << '\n';
            /******** ����� ��������� ********/
            cout << "���������...: ";
            cout << products[i]->category << '\n';
            /******** ����� �������� ********/
            cout << "��������...: ";
            cout << products[i]->name << '\n';
            cout << '\n';
        }
        bool (*check_function)(product_catalog*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
                                                           // � ����������� � �������� ��������� �������� ���� book_subscription*
        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ����� ������� � ��������� ����������\n";
        cout << "2) ����� ������� ������ 100 ������\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            delete products[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
   
 return 0;

}
